# onstop requirements

## Goals

- [hermetic](#hermetic)
- [reproducible](#reproducible)
- [multiplatform](#multiplatform)
- [fast](#fast)
- [cacheable](#cacheable)
- [distributed](#distributed)
- [high-level integration](#high-level-integration)
- [extensibility](#extensibility)


### Hermetic

Is the property of an operation, where the whole input and environment is
controlled. In the case of a build system, it means that running the software
in a different host will not affect how the code is build.

NO NETWORK IS AVAILABLE FOR CODE RUNNING ACTIONS. When something has to
be read from the network, it must be declared together with an authentication
hash. The content is downloaded, validated, stored in the cache and made
available to the action as a file. Some possible input types are:

- git (validated by commit hash)
- http (validated by sha256)
- docker image (validated by image hash)
- others: see "extensibility"

All inputs to a build action have to be tracked, as all the binaries necessary
to execute it. Anything not declared should not be available. Anything that
happen to be available on the machine are irrelevant, and can't be used and
network is not available. If for example a compiler is necessary, it has to be
declared, with the exact desired version. In this sense, any binary need to
execute an action, is an input to the action.

This will require some kind of container for running actions that include only
what was explicitly declared on it.

This property affects the design of the cache system. Specifically the cached
output of an action can only be reused if it was produced with the same inputs,
using the same binaries.

### Reproducible

Reproducibility is ensuring that the output of an action can be computed again
using the same input. Ideally, the action would be idempotent, meaning that
the same output is produced when the same input is provided. Sometimes this is
not possible, since some action will include for example dates on the generated
output. In that case, the action must at least guarantee that running an
action again will produce an equivalent output, meaning that they are
interchangeable.

### Multiplatform

The build system must be able to handle different CPU architectures and
operating systems. For example, the action that build a C++ file can be
executed on a MacOS/ARM or on a Linux/x64. In this case, the action would
declare the source code as a platform-independent input, the compiler as a
platform-local input. The OS/architecture of the output can be specified to
run locally (MacOS/ARM) or to run on on a Linux/x64 server, where it should
run. In this case, a C++ cross compiler would have to be used to run locally,
or the build action would have to be sent to another node, one that support
the platform constraints of the action.

Very special care has to be taken here:

- a compiler have a host and target architecture
- a binary have an architecture. When it is used on an action to be executed
  it must match the system running the action; when is used as data, this is
  not necessary (for example, we may be compressing a binary, in what case the
  compressor is being executed, and the binary is just data, so it don't have
  to match system)
- there is the need of a special "architecture independent" concept that we
  can use for plain data, of for output of code like python or node.

### Fast

The overhead of building the code locally must be minimized. Building code
with cold cache must take approximately the same time as running on this
build system, and it must use equivalent resources, in the case of a local-only
build.

In the case of distributed building there might be opportunities for trade-offs
between metrics, like resource usage and build time. For example, it might be
possible to use multiple machines and cache to reduce the wall-time of the
build. The specific requirements are not in the scope of this document and
must be designed separately.

### Cacheable

Build action execution output can be saved to cache, allowing avoiding the
execution of action in the future.

Points to consider:

- cheap actions: if the cost of running the action very small it may make
  sense to not store the result to cache;
- cost of action vs cost of cache: it may makes sense to compare the cost of
  fetching the cached item with the cost of running the action and changing
  the cache behavior;
- consider the possibility of storing not only the action output on the cache,
  but also additional information, like time to compute the action and the
  dependencies, or links to the cached actions that build the dependencies.
  There might be good opportunities to allow the code that do the planning and
  placement of actions to use the cache to execute the build faster by
  avoiding fetching from cache items that are faster to recompute or other
  cases that might be identified after the design of the system, during the
  implementation;
- offline work: if a developer is using a network cache, the workflow must
  not be disrupted by offline work or bad network.
- multilayer: the design must support multiple layers of cache; at least
  local cache + network cache.
- cleanup: must be part of the design


### Distributed

The build system should embrace and work together with other tools without
weakening the other properties. It must support the following scenarios:

1. solo developer working online or offline on Linux or MacOS;
2. CI single-node running rootless, possibly on container
3. CI with build farm
4. developer team backed by build farm

The two last scenarios require support for distributed build. Relevant points:

- security:
  - CI should NOT use cache values from developers;
  - developers may share cache or not; it's the team's decision;
- task placement: placing tasks to run must take into account:
  - the load of each node
  - the trade-off of faster execution on different node vs additional network
    transfer
  - the trade-off of additional resource usage vs lower wall-time of finishing
    the build;
- retries:
  - automatically retry action if it was caused by an issue with the build
    system (for example, node shutdown, some cases of OOM)
- kubernetes: it must be possible to run build cluster on kubernetes

### Flexible Integration

Code is frequently stored in git, sometimes on github. Compilers can be
blistering fast, like in the case of _go_ or slow and dumb, like c++. All
All these technologies give opportunities for specific types of optimization.
A good build system MUST take advantage of them.

Example scenario: one developer working a huge monorepo created a branch to
work one one of the services. The build graph for the service is like:

```txt
[go code]   [protobuf]   [github.com/gorilla/mux]  [hub.docker.com/_/scratch]
    |            |                   |                        |
    |            |                   |                        |
    |            V                   |                        |
    |     <proto compiler>           |                        |
    |            |                   |                        |
    |             -----              |                        |
    |                  V             |                        |
     ------------->  [go code] <-----                         |
    |                  |                                      |
    |                  |                                      |
    V                  V                                      |
<unit test>     <go compiler> <---> [go cache]                |
                       |                                      |
                       |                                      |
                       V                                      |
                 [my-service.bin]                             |
                       |                                      |
                       |                                      |
                       V                                      |
                   <buildah>  <-------------------------------
                       |
                       |
                       V
               my-service.tar.gz>     [other 100 services]  [integration tests]
                       |                        |                    |
                       |                        |                    |
                       V                        |                    |
                <kubernetes dev cluster>  <----- --------------------
```

The monorepo has hundreds of projects like that. The developer changed the
code for the service, committed the changed to git then asked to
build my-service.tar.gz. Some of the opportunities here:

- the git history can be used to search the cache; the versions can be
  efficiently compared to guide de decision to use cache
- `go ls-files` can be used to find what exactly what files are used by each
  service and each unit test; the output of this command can itself be cached
- the images fetched from dockerhub can be stored in the cache
- the `go build` cache can be managed by the tool
- the kubernetes objects can be marked with metadata, allowing determining
  if the deployed objects are up-to-date. It should then allow deploying and
  tracking versions.

In the case of `C++` compilers it may be better to build each source file
separately, then link it. The build system should provide something equivalent
to `ccache`. In the case of `go`, it may make more sense to provide all the
source code and call the compiler once.

### Extensibility

The core of the build system must be as small as possible. An extension system
must be provided, and it must be used to implement as much of the functionality
as technically possible.

Users of the build system will declare the extensions used, which will be
downloaded and stored on cache.

##
